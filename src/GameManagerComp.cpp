#include "stdafx.h"
#include "GameManagerComp.h"
#include "ReactorCore.h"
#include "ReactorTemperature.h"
#include "ReactorPower.h"
#include "ControlRod.h"
#include "GameOverlay.h"

using namespace Krawler;
using namespace Krawler::Input;

GameManagerComp::GameManagerComp(KEntity *entity)
	: KComponentBase(entity)
{
}

KInitStatus GameManagerComp::init()
{
	auto siren = ASSET().getSound("siren");
	KCHECK(siren);
	m_failSirenSound.setBuffer(*siren);
	m_failSirenSound.setLoop(true);
	m_failSirenSound.setVolume(50);

	auto success = ASSET().getSound("success_sound");
	KCHECK(success);
	m_successSound.setBuffer(*success);

	auto outsideRange = ASSET().getSound("outside_range");
	KCHECK(outsideRange);
	m_outsideRangeSound.setBuffer(*outsideRange);

	return KInitStatus::Success;
}

void GameManagerComp::tick()
{
	if (KInput::JustPressed(KKey::Escape))
	{
		GET_APP()->closeApplication();
	}

	if (KInput::JustPressed(KKey::P))
	{
		if (!m_isLevelActive && m_levelAttemptCount == 0)
		{
			m_isLevelActive = true;
			m_overlay->getEntity()->setActive(false);
			++m_levelAttemptCount;
			m_outsideRangeTimer.restart();
		}
	}

	if (KInput::JustPressed(KKey::R))
	{
		if (!m_isLevelActive)
		{
			resetGame();
			m_isLevelActive = true;
			m_overlay->getEntity()->setActive(false);
			++m_levelAttemptCount;
		}
	}

	if (KInput::JustPressed(KKey::Q))
	{
		if (!m_isLevelActive)
		{
			GET_APP()->closeApplication();
		}
	}

	spdlog::debug("Power percent {} ", m_reactorPower->getFillPercentage());

	if (m_isLevelActive)
	{
		// Failed from too hot
		if (m_reactorTemp->getFillPercentage() >= 100.0f)
		{
			m_isLevelActive = false;
			m_overlay->getEntity()->setActive(true);
			auto str = fmt::format("[ Survived for {}! ]\nTemperature caused core meltdown!\nPress [R] to retry or [Q] to quit", getSurvivalTimeString());
			m_overlay->setOverlayString(str, true);
			if (m_failSirenSound.getStatus() != sf::Sound::Status::Playing)
			{
				m_failSirenSound.play();
			}
		}
		else if (m_reactorPower->getFillPercentage() >= 100.0f)
		{
			m_isLevelActive = false;
			m_overlay->getEntity()->setActive(true);
			auto str = fmt::format("[ Survived for {}! ]\nPower output failure!\nPress [R] to retry or [Q] to quit", getSurvivalTimeString());
			m_overlay->setOverlayString(str, true);
		}

		const float upperY = m_upperBound->m_pTransform->getPosition().y;
		const float lowerY = m_lowerBound->m_pTransform->getPosition().y;
		// Inverted because upper is low y values, and lower is high y values
		// seriously bad naming occurs as your sleep depravation kicks in
		bool before = m_hasBecomeWithinRange;
		if (m_reactorPower->getFillYPosition() <= lowerY && m_reactorPower->getFillYPosition() >= upperY)
		{
			m_hasBecomeWithinRange = true;
			if (m_outsideRangeSound.getStatus() == sf::Sound::Status::Playing)
			{
				m_outsideRangeSound.stop();
			}
		}
		else
		{
			m_hasBecomeWithinRange = false;
		}

		if (m_hasBecomeWithinRange)
		{
			if (m_challengeTimer.getElapsedTime().asSeconds() >= m_survivalTimer)
			{
				pickNewBounds();
				m_hasBecomeWithinRange = false;
				m_successSound.play();
			}
		}
		else
		{
			m_challengeTimer.restart();
			if (m_outsideRangeSound.getStatus() != sf::Sound::Status::Playing)
			{
				m_outsideRangeSound.play();
			}

			if (before != m_hasBecomeWithinRange)
			{
				m_outsideRangeTimer.restart();
			}

			if (m_outsideRangeTimer.getElapsedTime() > sf::seconds(5.0f))
			{
				std::string failString;
				if (m_reactorPower->getFillYPosition() > lowerY)
				{
					failString = fmt::format("[ Survived for {}! ]\nReactor was below required \npower range for too long \n\n[R] to retry or [Q] to quit",
											 getSurvivalTimeString());
				}
				else if (m_reactorPower->getFillYPosition() < upperY)
				{
					failString = fmt::format("[ Survived for {}! ]\nReactor was above required \npower range for too long \n\n[R] to retry or [Q] to quit",
											 getSurvivalTimeString());
				}
				m_isLevelActive = false;
				m_overlay->getEntity()->setActive(true);
				m_overlay->setOverlayString(failString, true);
			}
		}
	}
}

void GameManagerComp::onEnterScene()
{
	auto reactorCoreEntity = GET_SCENE()->findEntity(GConsts::REACTOR_CORE_TAG);
	KCHECK(reactorCoreEntity);
	m_reactorCore = reactorCoreEntity->getComponent<ReactorCore>();
	KCHECK(m_reactorCore);

	auto reactorTemp = GET_SCENE()->findEntity(GConsts::REACTOR_TEMPERATURE_TAG);
	KCHECK(reactorTemp);
	m_reactorTemp = reactorTemp->getComponent<ReactorTemperature>();
	KCHECK(m_reactorTemp);

	auto reactorPower = GET_SCENE()->findEntity(GConsts::REACTOR_POWER_TAG);
	KCHECK(reactorPower);
	m_reactorPower = reactorPower->getComponent<ReactorPower>();
	KCHECK(m_reactorPower);

	auto go = GET_SCENE()->findEntity(GConsts::OVERLAY_TAG);
	KCHECK(go);
	m_overlay = go->getComponent<GameOverlay>();
	KCHECK(m_overlay);

	m_lowerBound = GET_SCENE()->findEntity(GConsts::LOWER_BOUND_TAG);
	KCHECK(m_lowerBound);

	m_upperBound = GET_SCENE()->findEntity(GConsts::UPPER_BOUND_TAG);
	KCHECK(m_upperBound);

	m_overlay->setOverlayString("Press 'P' to begin");

	auto entityList = GET_SCENE()->getAllocatedEntityList();

	uint64 crIdx = 0;
	for (auto &e : entityList)
	{
		if (auto comp = e->getComponent<ControlRod>())
		{
			m_controlRods[crIdx] = comp;
			++crIdx;
		}

		if (crIdx >= m_controlRods.size())
		{
			break;
		}
	}

	m_maxBoundYPos = GConsts::REACTOR_POWER_POSITION.y + GConsts::LONG_PANE_OUTLINE.y;
	m_minBoundYPos = m_maxBoundYPos + (GConsts::REACTOR_LONG_PANEL_SIZE.y - GConsts::LONG_PANE_OUTLINE.y);

	auto upperSize = m_upperBound->getComponent<Components::KCSprite>()->getOnscreenBounds();
	auto lowerSize = m_lowerBound->getComponent<Components::KCSprite>()->getOnscreenBounds();

	resetGame();
}

void GameManagerComp::resetGame()
{
	m_isLevelActive = false;
	m_overlay->getEntity()->setActive(true);
	for (auto &cr : m_controlRods)
	{
		cr->resetControlRod();
	}
	m_reactorTemp->setFillPercentage(0.0f);
	m_reactorPower->setFillPercentage(0.0f);
	m_reactorCore->resetCore();

	m_failSirenSound.stop();

	m_gamerTimer.restart();

	float xPosition = GConsts::REACTOR_POWER_POSITION.x + GConsts::REACTOR_LONG_PANEL_SIZE.x;
	const float size = (m_minBoundYPos - m_maxBoundYPos);
	const float centre = (m_maxBoundYPos + m_minBoundYPos) / 2.0f;
	m_boundsDifferencePercent = 0.2f * size;

	m_upperBound->m_pTransform->setPosition(xPosition, centre - m_boundsDifferencePercent);
	m_lowerBound->m_pTransform->setPosition(xPosition, centre + m_boundsDifferencePercent);
	m_hasBecomeWithinRange = false;
	m_outsideRangeTimer.restart();
}

void GameManagerComp::pickNewBounds()
{
	m_boundsDifferencePercent *= 0.8f;
	float upperY, lowerY;
	bool isAlreadyInRange = false;
	do
	{
		upperY = Maths::RandFloat(m_maxBoundYPos, m_minBoundYPos - m_boundsDifferencePercent);
		lowerY = upperY + m_boundsDifferencePercent;
		isAlreadyInRange = m_reactorPower->getFillYPosition() <= lowerY && m_reactorPower->getFillYPosition() >= upperY;
	} while (isAlreadyInRange);

	const float currentX = m_upperBound->m_pTransform->getPosition().x;
	m_upperBound->m_pTransform->setPosition(currentX, upperY);
	m_lowerBound->m_pTransform->setPosition(currentX, lowerY);
	m_outsideRangeTimer.restart();
}

std::string GameManagerComp::getSurvivalTimeString() const
{
	uint64 minutes, seconds; //milliseconds;
	minutes = KCAST(uint64, m_gamerTimer.getElapsedTime().asSeconds()) / 60lu;
	seconds = KCAST(uint64, m_gamerTimer.getElapsedTime().asSeconds()) % 60;
	// milliseconds = m_gamerTimer.getElapsedTime().asMilliseconds() % 100;

	return fmt::format("{:02}:{:02}", minutes, seconds);
}