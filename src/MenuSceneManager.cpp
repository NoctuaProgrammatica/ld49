#include "stdafx.h"
#include "MenuSceneManager.h"
#include "Constants.h"

using namespace Krawler;
using namespace Krawler::Components;
using namespace Krawler::Input;

MenuSceneManager::MenuSceneManager(KEntity *entity)
	: KComponentBase(entity)
{
}

KInitStatus MenuSceneManager::init()
{
	auto menuTex = ASSET().getTexture("menu_bg");
	KCHECK(menuTex);
	if (!menuTex)
	{
		return KInitStatus::MissingResource;
	}

	auto spr = new KCSprite(getEntity(), Vec2f(menuTex->getSize()));
	KCHECK(spr);

	spr->setTexture(menuTex);

	getEntity()->addComponent(spr);

	auto music = ASSET().getMusic("game_theme");
	KCHECK(music);
	music->play();
	music->setLoop(true);
	return KInitStatus::Success;
}

void MenuSceneManager::tick()
{
	if (KInput::JustPressed(KKey::Escape) || KInput::JustPressed(KKey::Q))
	{
		GET_APP()->closeApplication();
	}

	if (KInput::JustPressed(KKey::P))
	{
		if (!m_readyToTransition)
		{
			auto tex = ASSET().getTexture("instructions_bg");
			KCHECK(tex);
			getEntity()->getComponent<KCSprite>()->setTexture(tex);
			m_instructionsTimer.restart();
			m_readyToTransition = true;
		}
		else
		{
			GET_DIRECTOR().transitionToScene(GConsts::GAMEPLAY_SCENE_NAME);
		}
	}

	if (m_instructionsTimer.getElapsedTime() > sf::seconds(20.0f) && m_readyToTransition)
	{
		GET_DIRECTOR().transitionToScene(GConsts::GAMEPLAY_SCENE_NAME);
	}
}