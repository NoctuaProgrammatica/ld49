#version 130 

in vec4 gl_Color;
out vec4 gl_TexCoord[1];
out vec4 gl_FrontColor;
out vec4 gl_BackColor;

void main()
{
	// transform the vertex position
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;

	// transform the texture coordinates
	gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;
	// forward the vertex color
	gl_FrontColor = gl_Color; // sets bg to black
}