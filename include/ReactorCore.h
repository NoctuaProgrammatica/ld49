#pragma once

#include <Components\KCSprite.h>
#include "Constants.h"

class ControlRod;

class ReactorCore : public Krawler::KComponentBase
{
public:
	ReactorCore(Krawler::KEntity *entity);
	~ReactorCore() = default;

	virtual Krawler::KInitStatus init() override;
	virtual void onEnterScene() override;
	virtual void tick() override;

	float getTemperature() const { return m_coreTemperature; }
	float getPower() const { return m_powerOutput; }
	void resetCore();

private:
	Krawler::Components::KCSprite &m_sprite;

	std::array<ControlRod *, GConsts::CONTROL_ROD_COUNT> m_controlRods = {nullptr, nullptr, nullptr};
	float m_reactionRate;
	float m_coreTemperature;
	float m_powerOutput;

	// Constants used in reactor (Not tied to real world physics, entirely gameplay oriented)
	float m_reactionRateConst;
	float m_tempConst;
	float m_powerConst;

	float m_rateOfReactionCap;
	float m_coreTempDecay;
};