#pragma once 

#include <Krawler.h>
#include <KApplication.h>
#include <KComponent.h>
#include <AssetLoader/KAssetLoader.h>
#include <Input/KInput.h>
#include <Maths/KMaths.hpp>

#include <spdlog/spdlog.h>
#include <vector>