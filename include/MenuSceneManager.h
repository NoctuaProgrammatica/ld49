#pragma once

class MenuSceneManager : public Krawler::KComponentBase
{
public:
	MenuSceneManager(Krawler::KEntity *entity);
	~MenuSceneManager() = default;

	virtual Krawler::KInitStatus init() override;
	virtual void tick() override;

private:
	sf::Clock m_instructionsTimer;
	bool m_readyToTransition =false;
};
